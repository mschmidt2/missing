#!/bin/sh

kerneloscope log --limit 0 --tree rhel-8.6 --exclude-tree rhel-9.2 --path 'drivers/(net|infiniband)' | while read cid _; do
	bz=$(git show -s --no-decorate "$cid" | grep '^    Bugzilla:')
	if [ $? != 0 ]; then
		echo "No BZ line in $cid"
		exit 1
	fi

	bz="${bz#    Bugzilla: }"
	bz="${bz##*/}"
	bz="${bz##*=}"

	r9bz=$(bugzilla query -i --from-url "https://bugzilla.redhat.com/buglist.cgi?classification=Red%20Hat&columnlist=product%2Ccomponent%2Cassigned_to%2Cstatus%2Csummary%2Clast_change_time%2Cseverity%2Cpriority&component=kernel&f1=cf_devel_whiteboard&list_id=13075566&o1=regexp&order=status%2C%20priority%2C%20assigned_to%2C%20id%2C%20&product=Red%20Hat%20Enterprise%20Linux%209&query_format=advanced&v1=rhel86commit%5C%28.%2A$cid")

	subject=$(git show -s --format='%s' $cid)
	echo "$cid;$bz;$r9bz;$subject"
done
